from django.contrib import admin
from .models import Peralatan
# Register your models here.
@admin.register(Peralatan)
class PeralatanAdmin(admin.ModelAdmin):
    list_display = ['nama_alat','kategori','harga','berat','keterangan',
                    'tgl_input','user']
    list_filter = ['nama_alat','kategori','harga','user']
    search_fields = ['nama_alat','kategori','user']

